# Transcript of Pepper&Carrot Episode 17 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 17: A Fresh Start

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|But Pepper... Come back...
Pepper|2|False|NO! I'M LEAVING!!
Pepper|3|False|You don't teach real witchcraft! I'm going - to the witches of Ah!
Sound|4|False|Wo oo sh !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Alright then, off to the land of the setting moons
Pepper|2|False|Shichimi can tell us how to join the witches of Ah
Pepper|3|False|Carrot, get out the map and compass: it's too cloudy to see where I'm going
Pepper|5|True|DRATS!
Pepper|6|True|Turbulence!
Pepper|7|False|HOLD ON!
Pepper|9|False|Oh no !!!
Sound|8|False|BrrOoooOoo! !|nowhitespace
Writing|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Cr rAsh ! !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|6|False|Aah! !|nowhitespace
Monster|5|False|Wah! !|nowhitespace
Pepper|1|False|Above all, don't panic, Carrot...
Cayenne|2|True|"A good dark stare is worth a dozen useless curses! ...
Pepper|7|False|Personally, I would've preferred to learn a few good attack spells; but anyway...
Pepper|8|True|shoot... no more broom or equipment,
Pepper|9|False|this is going to be a long trip...
Cayenne|3|True|...Stare them down.
Cayenne|4|False|Dominate them! "

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|4|False|"To know the edible plants is to know where to find ready-made potions against hunger!"
Pepper|6|False|If only she had taught me a single one!
Pepper|2|True|I'm totally worn out too Carrot...
Pepper|3|False|And it's been days since we've eaten anything.
Carrot|1|False|Groo
Pepper|5|True|But real potions?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thyme|1|False|"A true witch of Chaosah needs neither compass nor map. A starry sky will suffice"
Pepper|3|True|Cheer up Carrot !
Pepper|4|False|Look, we've arrived!
Pepper|2|False|... I would've preferred a real course in divination!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|7|False|License : Creative Commons Attribution 4.0, Software: Krita, G'MIC, Inkscape on Ubuntu
Credits|6|False|Based on the Hereva universe created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Narrator|4|False|- FIN -
Credits|5|False|06/2016 - www.peppercarrot.com - Art & Scenario : David Revoy - English Translation : Alex Gryson
Pepper|2|False|Now you know the whole story.
Shichimi|3|False|...and you tell me that you're here because they haven't taught you anythin g ?
Pepper|1|True|...so there you are.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 719 Patrons:
Credits|2|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
