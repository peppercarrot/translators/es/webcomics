#!/bin/bash
#
#  validate-transcripts.sh
#
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2019 GunChleoc <fios@foramnagaidhlig.net>
#
#  Script for validating Markdown transcript contents and their generation
#

scriptdir="0_transcripts"

# Move up if we're not in the base directory.
if [ -d "../$scriptdir" ]; then
    pushd ..
fi

# Run unit tests

0_transcripts/run_tests.py
if [[ ! $? -eq 0 ]] ; then
    echo " "
    echo "ERROR: Unit tests failed"
    exit 1
fi

# Run integration tests
declare -i errors; errors=0

# Find episodes/locales that have transcripts and validate them
for episodedir in *; do
    if [ -d "${episodedir}" ] ; then
        # We have a directory. Search for any locales
        for localedir in ${episodedir}/lang/*; do
            if [ -d "${localedir}" ] ; then
                # Only process directories that have transcripts in them
                for mdfile in ${localedir}/ep*_transcript.md; do
                    if [ -f "$mdfile" ] ; then
                        # There is a transcript for this episode and locale.
                        # Validate it and then break to the next locale.

                        # Get the locale from the cvs file's path
                        locale=$(basename $(dirname $mdfile))

                        # Regenerate transcript to check if it will run through
                        echo "Validating transcript for $episodedir $locale"
                        # Output is noisy, so redirecting it to nowhere
                        $scriptdir/extract_text.py $episodedir $locale > /dev/null
                        if [[ ! $? -eq 0 ]] ; then
                            echo " "
                            echo "ERROR: Updating transcript encountered an error for $episodedir $locale"
                            # Run it again to show the output
                            $scriptdir/extract_text.py $episodedir $locale
                            errors+=1
                            echo " "
                        fi

                        # Generate HTML to check if it will run through
                        echo "Validating HTML generation for $episodedir $locale"
                        # Output is noisy, so redirecting it to nowhere
                        $scriptdir/extract_to_html.py $episodedir $locale > /dev/null
                        if [[ ! $? -eq 0 ]] ; then
                            echo " "
                            echo "ERROR: Generating HTML encountered an error for $episodedir $locale"
                            # Run it again to show the output
                            $scriptdir/extract_to_html.py $episodedir $locale
                            errors+=1
                            echo " "
                        fi
                        break
                    fi
                done
            fi
        done
    fi
done


if [[ (${errors} > 0 )]] ; then
    echo "#####################"
    echo "  Found ${errors} error(s)"
    echo "#####################"
    exit 1
fi
echo " "

# Ensure that regenerating the transcripts did not introduce any changes
echo " "
if [ -z "$(git diff)" ]; then
    echo "#############################"
    echo "  All transcripts are OK :)"
    echo "#############################"
else
    echo "###################################"
    echo "  Some transcripts need updating!"
    echo "###################################"
    git status -s
fi
