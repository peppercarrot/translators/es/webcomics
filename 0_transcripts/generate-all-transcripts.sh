#!/bin/bash
#
#  generate-all-transcripts.sh
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2019 GunChleoc <fios@foramnagaidhlig.net>
#
#  Script for creating CSV transcripts and their HTML output for all
#  episodes and locales
#

scriptdir="0_transcripts"

# Move up if we're not in the base directory.
if [ -d "../$scriptdir" ]; then
    pushd ..
fi

declare -i md_errors; md_errors=0
declare -i html_errors; html_errors=0

# Find all episodes/locales and generate transcripts
for episodedir in *; do
    if [ -d "${episodedir}" ] ; then
        # We have a directory. Search for any locales
        for localedir in ${episodedir}/lang/*; do
            if [ -d "${localedir}" ] ; then
                # Only process directories that have svg files in them
                for svgfile in ${localedir}/*.svg; do
                    if [ -f "$svgfile" ] ; then
                        # There is an svg file for this episode and locale.
                        # Generate transcripts it and then break to the next locale.

                        # Get the locale from the SVG file's path
                        locale=$(basename $(dirname $svgfile))

                        # Regenerate transcript Markdown and HTML output.
                        # Script output is noisy, so redirecting it to nowhere.
                        echo "Generating HTML transcript for $episodedir $locale"
                        $scriptdir/extract_text.py $episodedir $locale > /dev/null
                        if [[ ! $? -eq 0 ]] ; then
                            echo "ERROR: Updating transcript encountered an error for $episodedir $locale"
                            md_errors+=1
                        fi
                        $scriptdir/extract_to_html.py $episodedir $locale > /dev/null
                        if [[ ! $? -eq 0 ]] ; then
                            echo "WARNING: Generating HTML encountered an error for $episodedir $locale"
                            html_errors+=1
                        fi
                        break
                    fi
                done
            fi
        done
    fi
done

if [[ (${md_errors} > 0 )]] ; then
    echo "###################################"
    echo "  Found ${md_errors} Markdown error(s)"
    echo "###################################"
fi

if [[ (${html_errors} > 0 )]] ; then
    echo "###################################"
    echo "  Found ${html_errors} HTML warning(s)"
    echo "###################################"
else
    echo "###################################"
    echo "  All transcripts are OK :)"
    echo "###################################"
fi

if [[ (${md_errors} > 0 ) ]] ; then
    exit 1
fi
