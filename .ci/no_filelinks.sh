#!/bin/bash

# Script for validating that Inkscape hasn't generated file:// links

echo "##############################################################"
echo "Searching SVG files for file:/// links"

# Move up if we're not in the base directory.
if [ -d "../.ci" ]; then
    pushd ..
fi

errors=0

# Walk all SVG files and check them for file:///
for svgfile in */lang/*/*.svg; do
    if [ -f "$svgfile" ] ; then
        lines=`cat $svgfile`
        for line in $lines; do
            if [[ $line == *"file:///"* ]]; then
                echo " "
                echo "ERROR: Found file:/// link in $svgfile - please fix!"
                echo "    $line"
                errors=1
            fi
        done
    fi
done

echo " "
if [ $errors -eq 1 ]; then
    echo "##############################################################"
    echo "File links should look like this:"
    echo xlink:href="../gfx_Pepper-and-Carrot_by-David-Revoy_<panel>.png"
    echo "##############################################################"
    exit 1
else
    echo "Done, all clean!"
    echo "##############################################################"
    exit 0
fi
