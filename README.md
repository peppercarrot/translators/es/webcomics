﻿# Pepper&Carrot webcomics

This repository contains all the files necessary to translate the [Pepper&Carrot](https://www.peppercarrot.com) webcomics.

## Documentation

To contribute to the project, you can read [the full documentation here](https://www.peppercarrot.com/static14/documentation)
, but in a nutshell you just need to install the fonts (available in the subfolder [`fonts`](fonts)), get the repository and translate the SVGs inside the episode folder with Inkscape.

## References

To keep consistency for the names of characters and place accross all the webcomic's episodes, you can edit the LibreOffice spreadsheet file [translation-names-references.fods](translation-names-references.fods) at the root of the repository.

## Credits

Read [AUTHORS.md](AUTHORS.md) in the root of this repository's tree for the full attribution. This file is also included in the main website's ["Author" category](https://www.peppercarrot.com/static7/author).

## Artwork sources

This repository contains only low resolution artworks for editing the translations. Do not edit or open merge requests for the artwork here. For rendering the page at 300ppi, use the high resolution graphics available [from the official website](https://www.peppercarrot.com/0_sources/).

## Fonts

The open fonts necessary to translate Pepper&Carrot are in the [`fonts`](https://framagit.org/peppercarrot/fonts) repository.
For documentation and their license, check their [`README.md`](https://framagit.org/peppercarrot/fonts/blob/master/README.md).

## License

Content in this repository is licensed under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License, except for the files in the `fonts/` directory, which are released under their own separate license agreements.

By submitting content to this repository, one agrees to the contributor's terms at the bottom of [CONTRIBUTING.md](CONTRIBUTING.md).

## Episode metadata

Each episode contains a metadata file called `info.yaml`. The format is documented in [INFO-YAML.md](INFO-YAML.md).
