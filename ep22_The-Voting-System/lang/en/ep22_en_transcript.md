# Transcript of Pepper&Carrot Episode 22 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 22: The Voting System

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Our great magic contest can finally commence!
Writing|2|False|Magic Contest

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|And thanks to our brilliant engineers, you too will be able to participate!
Mayor of Komona|2|False|Yes, yes, my friends, feast your eyes on these technological wonders our hosts and hostesses will provide you!
Mayor of Komona|3|False|The green button gives a point to a candidate, the red button removes one. It's you who decides!
Mayor of Komona|4|True|"And the jury?", I hear you ask.
Mayor of Komona|5|False|Don't worry, we've thought of everything!
Mayor of Komona|6|False|Each juror has a special remote, able to give or take a hundred points with a single press!
Pepper|7|False|Wow!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|5|False|…50,000 Ko!
Mayor of Komona|1|False|And the icing on the cake— the scores will appear directly above the participant's head!
Mayor of Komona|3|False|The three witches who obtain the best scores will proceed to the finals!
Mayor of Komona|4|True|Don't forget, the grand prize winner will take home the amazing sum of…
Writing|2|False|1337
<hidden>|0|False|Edit this one, all others are linked
Audience|6|False|Clap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carrot|2|False|Tap
Pepper|3|False|Such a great concept, isn't it Carrot?
Pepper|4|True|Innovative…
Pepper|5|True|Amusing…
Pepper|6|True|Democratic…
Pepper|7|False|… the perfect system!
Pepper|8|True|We no longer need experts to judge for quality!
Pepper|9|False|What progress! We really do live in a fantastic era!
Mayor of Komona|10|False|Everyone have their remote?
Audience|11|False|Great!
Audience|12|False|Yes!
Audience|13|False|Yeah!
Audience|14|False|Yes!
Audience|15|False|Yes!
Mayor of Komona|16|True|Good!
Mayor of Komona|17|True|Let the contest…
Mayor of Komona|18|False|BEGIN!!
<hidden>|0|False|Edit this one, all others are linked
Audience|1|False|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|And with Camomile we start the event!
Sound|2|False|Dgiiioo…
Camomile|3|False|SYLVESTRIS!
Sound|4|False|Bam!
Audience|5|True|Tap
Audience|6|True|Tap
Audience|7|True|Tap
Audience|8|True|Tap
Audience|9|True|Tap
Audience|10|True|Tap
Audience|11|False|Tap

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|2|False|Camomile obtains a very good score! It is now Shichimi's turn!
Writing|1|False|5861
Shichimi|4|False|LUX…
Sound|5|False|Fii iiiii zz!!|nowhitespace
Shichimi|6|False|MAXIMA!
Audience|7|False|Aah!!
Audience|8|False|Ow!!
Audience|9|False|My eyes!!!
Pepper|12|False|Carrot… give her a green thumb anyway; she's our friend after all…
Carrot|13|False|Tap
Audience|10|True|Boo!
Audience|11|False|Booo!
Audience|14|True|Booo!
Audience|15|False|Booo!
Mayor of Komona|17|False|Ah, it would appear the crowd was not dazzled by this "brilliant" display! We now turn to Spirulina!
Writing|16|False|-42
Audience|18|True|Boooo!
Audience|19|True|Booo!
Audience|20|True|Booo!
Audience|21|False|Boo!
<hidden>|0|False|Edit this one, all others are linked
Audience|3|False|Clap

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spirulina|1|False|RELEASUS KRAKENIS!
Sound|2|False|Www loo!|nowhitespace
Sound|3|False|S plaa shh !|nowhitespace
Mayor of Komona|5|False|Beautiful! Powerful! Spirulina gains the lead! Coriander, over to you!
Writing|4|False|6225
Spirulina & Durian|6|False|Tap
Coriander|8|False|MORTUS REDITUS!
Sound|9|False|Groo wooo !|nowhitespace
Mayor of Komona|11|False|Oh, it seems that skeletons are out of fashion… Now for our dear Saffron!
Writing|10|False|2023
<hidden>|0|False|Edit this one, all others are linked
Audience|7|False|Clap

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|1|False|Grrr! There's no way I'll let myself do worse than Spirulina! It's time to give it my all!
Saffron|2|False|Stand back, Truffel!
Sound|3|False|Fr rrshh !|nowhitespace
Sound|4|False|Fr rrshh !|nowhitespace
Sound|5|False|Krchh!
Truffel|6|False|Meow!
Saffron|7|False|SPIRALIS
Saffron|8|False|FLAMA aaaa aaah!|nowhitespace
Sound|9|False|Fw wwooo oshh !|nowhitespace
Sound|10|False|Swwwiiip!
Sound|11|False|Fs ssh !|nowhitespace
Sound|12|False|Paf!
Saffron|13|False|?!!
Sound|14|False|Fs ssh !|nowhitespace
Sound|15|False|Fs ssh !|nowhitespace
Saffron|16|False|Aiiiiiee! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffron|1|False|Oh no… How embarrassing!
Saffron|3|False|?!
Writing|4|True|14
Writing|5|False|849
Pepper|6|False|Tap
Lord Azeirf|7|True|Tap
Lord Azeirf|8|True|Tap
Lord Azeirf|9|False|Tap
Writing|10|True|18
Writing|11|False|231
Mayor of Komona|12|True|Please... please! A little decorum!
Mayor of Komona|13|False|Shichimi and Coriander are eliminated!
Mayor of Komona|14|False|Camomile, Spirulina and Saffron have qualified for the finals!
<hidden>|0|False|Edit this one, all others are linked
Audience|2|False|Clap

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Ladies, are you ready for one more challenge?
Mayor of Komona|2|False|Over to you!
Writing|3|True|1 5|nowhitespace
Writing|4|False|703
Writing|5|True|19
Writing|6|False|863
Writing|7|True|1 3|nowhitespace
Writing|8|False|614
Sound|10|False|Plonk!
Pepper|11|False|I take back everything I said about this system...
Lord Azeirf|12|True|Tap
Lord Azeirf|13|True|Tap
Lord Azeirf|14|False|Tap
Narrator|15|False|- FIN -
<hidden>|0|False|Edit this one, all others are linked
Audience|9|False|Clap

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|05/2017 - www.peppercarrot.com - Artwork & Scenario : David Revoy - Translation: Alex Gryson
Credits|2|False|Dialogue Improvements: Nicolas Artance, Valvin, Craig Maloney.
Credits|3|False|Based on the Hereva universe created by David Revoy with contributions from Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|4|False|Software : Krita 3.1.3, Inkscape 0.92.1 on Linux Mint 18.1
Credits|5|False|License : Creative Commons Attribution 4.0
Credits|6|False|Pepper&Carrot is entirely free(libre), open-source, and sponsored thanks to the patronage of its readers. For this episode, thanks go to the 864 Patrons:
Credits|7|False|You too can become a patron of Pepper&Carrot on www.patreon.com/davidrevoy
